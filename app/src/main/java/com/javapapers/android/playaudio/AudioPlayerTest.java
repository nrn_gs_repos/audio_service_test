package com.javapapers.android.playaudio;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

/**
 * Created by NishantThite on 27/08/15.
 */
public class AudioPlayerTest  extends View {

    Context apContext;
    SeekBar audioSeekBar;
    BroadcastReceiver receiver;
    ProgressDialog pdInAudioPlayer;
    Activity ownerActivity;
    public TextView buffer;
    public TextView totalTime;
    public TextView currentTime;

    public AudioPlayerTest(Context context) {
        super(context);
        this.apContext = context;
    }

    public void playAudioFile(){
        showMyPD();
        Log.d(PlayAudio.LOG_ID,"........PLAY clicked......");
        Intent objIntent = new Intent(apContext, PlayAudio.class);
        ownerActivity.startService(objIntent);
    }

    public void initBroadcastReceiver() {
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String str = intent.getStringExtra(PlayAudio.NISHANT_MESSAGE);
                String str1 = intent.getStringExtra(PlayAudio.NISHANT_MESSAGE_2);
                String str2 = intent.getStringExtra(PlayAudio.NISHANT_MESSAGE_3);
                audioSeekBar.setMax(Integer.parseInt(str2));
                myBroadcastAction(str,str2);
                myBroadcastAction1(str1);
                myBroadcastAction2(str, str2);
            }
        };
        Log.d(PlayAudio.LOG_ID,"........BroadcastReceiver initialized ......");
    }

    public void registerBroadcastReceiver(){
        LocalBroadcastManager.getInstance(apContext).registerReceiver((receiver),
                new IntentFilter(PlayAudio.NISHANT_NOTIFICATION)
        );
        Log.d(PlayAudio.LOG_ID, "........BroadcastReceiver registered ......");
    }

    void myBroadcastAction(String str, String str2){
        if (pdInAudioPlayer != null){
            pdInAudioPlayer.dismiss();
            int timeDuration = Integer.parseInt(str2);

        }
        Log.d(PlayAudio.LOG_ID, "........myBroadcastAction : " + str + "  ......");
        audioSeekBar.setProgress(Integer.parseInt(str));
    }

    void myBroadcastAction1(String str){

        Log.d(PlayAudio.LOG_ID, "........myBroadcastAction : " + str + "  ......");
        buffer.setText(str);
    }

    void myBroadcastAction2(String str, String str2){
        long timeDuration = Long.parseLong(str2);
        long position = Long.parseLong(str);
        Log.d(PlayAudio.LOG_ID, "........myBroadcastAction : " + str + "  ......");
        totalTime.setText(getTimeString(timeDuration));
        currentTime.setText(getTimeString(position));
    }


    private String getTimeString(long millis) {
        StringBuffer buf = new StringBuffer();

        int hours = (int) (millis / (1000 * 60 * 60));
        int minutes = (int) ((millis % (1000 * 60 * 60)) / (1000 * 60));
        int seconds = (int) (((millis % (1000 * 60 * 60)) % (1000 * 60)) / 1000);

        buf
                .append(String.format("%02d", hours))
                .append(":")
                .append(String.format("%02d", minutes))
                .append(":")
                .append(String.format("%02d", seconds));

        return buf.toString();
    }
    void showMyPD(){
        pdInAudioPlayer = new ProgressDialog(apContext);
        pdInAudioPlayer.setMessage("Buffering..");
        pdInAudioPlayer.setCancelable(false);
        pdInAudioPlayer.show();
    }

}
