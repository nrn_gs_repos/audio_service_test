package com.javapapers.android.playaudio;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends Activity {
    public static SeekBar audioSeekBar;

    AudioPlayerTest audioPlayerTest;

    TextView textView , currentTime, totalTime;


    @Override
    public void onCreate(Bundle savedInstanceState) {

        Log.d(PlayAudio.LOG_ID,"........in MainActivity  ......");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        audioSeekBar = (SeekBar) findViewById(R.id.audioSeekBarId);
        audioPlayerTest = new AudioPlayerTest(this);
        textView = (TextView) findViewById(R.id.bufferring);
        currentTime = (TextView) findViewById(R.id.currentTime);
        totalTime = (TextView) findViewById(R.id.totalTime);
        basicSetupForAudioView();
    }

    void basicSetupForAudioView(){
        audioPlayerTest.initBroadcastReceiver();
        audioPlayerTest.registerBroadcastReceiver();
        audioPlayerTest.audioSeekBar = this.audioSeekBar;
        audioPlayerTest.buffer = this.textView;
        audioPlayerTest.currentTime = this.currentTime;
        audioPlayerTest.totalTime = this.totalTime;
        audioPlayerTest.ownerActivity = this;
    }


    @Override
    protected void onStart() {
        super.onStart();
    }


    public void playAudio(View view) {

        audioPlayerTest.playAudioFile();
    }
    
    public void stopAudio(View view) {
        audioSeekBar.setProgress(0);
        PlayAudio.bufferPercent = 0;
    	Intent objIntent = new Intent(this, PlayAudio.class);
        Log.d(PlayAudio.LOG_ID,"........STOP clicked......");
	    stopService(objIntent);
    }

    public void pauseAudio(View view) {
       // audioSeekBar.setProgress();
//        Intent objIntent = new Intent(this, PlayAudio.class);
//        Log.d(PlayAudio.LOG_ID,"........STOP clicked......");
//        stopService(objIntent);
          PlayAudio.onPause();
    }
}