package com.javapapers.android.playaudio;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.SeekBar;

public class PlayAudio extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener,
        SeekBar.OnSeekBarChangeListener, MediaPlayer.OnBufferingUpdateListener
{
    int timeElapsed;
    public static int bufferPercent = 0;
    boolean bufferingInProgress = true;
	public static final String LOG_ID = "HeyPrabhu";
	static  MediaPlayer mp;
    LocalBroadcastManager broadcaster;
    private android.os.Handler durationHandler = new android.os.Handler();
    private int mediaFileLengthInMilliseconds;
    static final public String NISHANT_NOTIFICATION = "com.javapapers.android.playaudio.NISHANT_NOTIFICATION";

    static final public String NISHANT_MESSAGE = "SEEKBAR_PROGRESS";
    static final public String NISHANT_MESSAGE_2 = "BUFFER_PROGRESS";
    static final public String NISHANT_MESSAGE_3 = "DURATION";



    public void sendResult(String message, String bufferPercent, String duration) {
        Intent intent = new Intent(NISHANT_NOTIFICATION);
        if(message != null && bufferPercent !=null)
        intent.putExtra(NISHANT_MESSAGE, message);
        intent.putExtra(NISHANT_MESSAGE_2, bufferPercent);
        intent.putExtra(NISHANT_MESSAGE_3, duration);
        broadcaster.sendBroadcast(intent);
    }

    public void onCreate(){
	    super.onCreate();
        broadcaster = LocalBroadcastManager.getInstance(this);
	    Log.d(LOG_ID, "Service Started!");
	}

    void playMyMediaPlayer(){
        Log.d(LOG_ID, " In playMyMediaPlayer");
        try {
            if (mp == null) {
                mp = new MediaPlayer();
                mp.setOnPreparedListener(this);
                mp.setOnBufferingUpdateListener(this);

            }
            else
                mp.reset();

            mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mp.setDataSource("http://gostories.co.in/Sandeep/prastawana.mp3");

            mp.prepareAsync();
        }

        catch(Exception e) {
            String errStr = "GS Error-41 : " + e.getMessage();
            Log.e(LOG_ID, errStr);
        }
    }

    private Runnable updateSeekBarTime = new Runnable() {
        public void run() {
            try{
                 timeElapsed = 0;
                if (mp != null && mp.isPlaying()) {
                    //get current position
                    timeElapsed = mp.getCurrentPosition();
                    //set seekbar progress
                    //audioSeekBar.setProgress((int) timeElapsed);
                    //set time remaing
                    // double timeRemaining = finalTime - timeElapsed;
                    // playTextView.setText(String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes((long) timeElapsed), TimeUnit.MILLISECONDS.toSeconds((long) timeElapsed) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) timeElapsed))));

                    //repeat yourself that again in 100 miliseconds
                    durationHandler.postDelayed(this, 100);
                } else{
                    // audioSeekBar.setProgress(0);
                }
                sendResult(""+timeElapsed,""+bufferPercent,""+mediaFileLengthInMilliseconds);

            }catch (Exception e){
                if (mp != null){
                    mp.release();
                    mp = null;
                }
                durationHandler.removeCallbacks(updateSeekBarTime);
                String errStr = "GS Error-42 : " + e.getMessage();
                Log.e(LOG_ID, errStr);
            }

        }
    };


	public int onStartCommand(Intent intent, int flags, int startId){
        playMyMediaPlayer();
	    return 1;
	}

	public void onStop(){
		mp.stop();
		mp.release();
    }
	
	public static void onPause()
    {
        if (mp != null) {
            mp.pause();
            mp.release();
        }
	}
	
	public  void onDestroy(){
        Log.e(LOG_ID, "In on Destroy");



            durationHandler.removeCallbacks(updateSeekBarTime);
            mp.stop();
            mp.release();
            Log.e(LOG_ID, "Destroy completed in codition check");


        mp = null;
        Log.e(LOG_ID, "Destroy completed");
	}

	@Override
	public IBinder onBind(Intent objIndent) {
	    return null;
	}



    public void onPrepared(MediaPlayer mp) {
        Log.d(LOG_ID, " In onPrepared");
        mediaFileLengthInMilliseconds = mp.getDuration();
        Log.e(LOG_ID,"Duration"+ mediaFileLengthInMilliseconds);
       MainActivity.audioSeekBar.setOnSeekBarChangeListener(this);
        mp.start();
        durationHandler.postDelayed(updateSeekBarTime, 200);
    }

    public boolean onError(MediaPlayer mp, int what, int extra) {

        String errorStr = "MediaPlayer onError : "+ what+" and "  + extra;
        Log.e(LOG_ID, errorStr);
        //mainActivityObj.errorTextView.setText(errorStr);


//        audioSeekBar.setProgress(0);
//        bufferPercent = 0 ;
//        audioSeekBar.setSecondaryProgress(bufferPercent);

        mp.reset();
        // streamAudioPlayer();

        return false;
    }

    public void onCompletion(MediaPlayer mp) {
         mp.reset();
        //Toast.makeText(getApplicationContext(), "Completed", Toast.LENGTH_LONG).show();
    }


    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if(fromUser) {
            // this is when actually seekbar has been seeked to a new position
            if (mp != null) {
//               if(mp.isPlaying() == false){
//                    mp.start();
//                }

               if(bufferPercent ==0) {
                   seekBar.setProgress(0);
               }
                else {

                   mp.seekTo(progress);
                   timeElapsed = progress;
                   System.out.println("Buffering: " + bufferPercent);
               }
            }else{
                seekBar.setProgress(0);
            }
        }
    }

    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        // TODO Auto-generated method stub
        bufferPercent = percent;
        if (percent < 0 && percent <= 100) {
            //System.out.println("Doing math: (" + (Math.abs(percent)-1)*100.0 + " / " +Integer.MAX_VALUE+ ")" );

            bufferPercent = (int) Math.round((((Math.abs(percent)-1)*100.0/Integer.MAX_VALUE)));

           // durationHandler.postDelayed(updateSeekBarTime, 200);
        }
        //progressBar.setProgress(percent);
        Log.i("Buffering! " , " " +bufferPercent );
        System.out.println("Buffering: " +bufferPercent);

    }


//    private Runnable updateBufferTime = new Runnable() {
//        public void run() {
//            try{
//                int timeElapsed = 0;
//                if (bufferingInProgress == true) {
//                    timeElapsed = bufferPercent;
//                    // audioSeekBar.setIndeterminate(false);
//                    // mainActivityObj.errorTextView.setText(updateMsg);
//                    if (bufferPercent == 100) {
//                        bufferingInProgress = false;
//                }
//
//            }
//                sendResult("" + timeElapsed, "" + bufferPercent);
//            }
//
//            catch (Exception e){
//                if (mp != null){
//                    mp.release();
//                    mp = null;
//                }
//                durationHandler.removeCallbacks(updateBufferTime);
//                String errStr = "GS Error-43 : " + e.getMessage();
//                Log.e(LOG_ID, errStr);
//            }
//
//        }
//    };
}
