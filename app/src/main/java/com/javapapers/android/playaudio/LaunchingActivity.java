package com.javapapers.android.playaudio;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import android.media.MediaPlayer.OnPreparedListener;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.TextView;

import java.io.IOException;

/**
 * Created by Admin_2 on 27-08-2015.
 */
public class LaunchingActivity extends Activity //implements OnPreparedListener, MediaController.MediaPlayerControl
{

    //private static final String TAG = "AudioPlayer";
   // public static final String AUDIO_FILE_NAME = "Walk";
    private MediaPlayer mediaPlayer;
    private MediaController mediaController;
    private String audioFile;
    private Handler handler = new Handler();
    Button next;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        next = (Button) findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(LaunchingActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });




    }

    void  setUpMediaPlayerAndConroller(){
        audioFile = "http://gostories.co.in/Sandeep/stories/waras.mp3";

        try {

            if (mediaPlayer == null)
                mediaPlayer = new MediaPlayer();
            else
                mediaPlayer.reset();

           // mediaPlayer.setOnPreparedListener(this);

            mediaController = new MediaController(this);
            mediaPlayer.setDataSource(audioFile);
            mediaPlayer.prepareAsync();

        } catch (IOException e) {
            Log.e(PlayAudio.LOG_ID, "Could not open file " + audioFile + " for playback.", e);
        }
    }

    public void onPrepared(MediaPlayer paramMediaPlayer) {
        Log.d(PlayAudio.LOG_ID, "onPrepared");
       // mediaController.setMediaPlayer(this);
        mediaController.setAnchorView(findViewById(R.id.main_audio_view));
        mediaPlayer.start();
        mediaController.show(0);
    }

    /* -------------------- Media Controller ------------------------ */
    public void start() {
        mediaPlayer.start();
    }

    public void pause() {
        mediaPlayer.pause();
    }

    public int getDuration() {
        return mediaPlayer.getDuration();
    }

    public int getCurrentPosition() {
        return mediaPlayer.getCurrentPosition();
    }

    public void seekTo(int pos) {
        mediaPlayer.seekTo(pos);
    }

    public boolean isPlaying() {
        return mediaPlayer.isPlaying();
    }

    public int getBufferPercentage() {
        return 0;
    }

    public boolean canPause() {
        return true;
    }

    public boolean canSeekBackward() {
        return false;
    }

    public boolean canSeekForward() {
        return false;
    }
}
